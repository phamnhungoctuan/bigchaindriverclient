<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

-   [Ed25519Keypair][1]
    -   [Parameters][2]
    -   [Properties][3]
-   [Connection][4]
    -   [Parameters][5]
    -   [getBlock][6]
        -   [Parameters][7]
    -   [getTransaction][8]
        -   [Parameters][9]
    -   [listBlocks][10]
        -   [Parameters][11]
    -   [listOutputs][12]
        -   [Parameters][13]
    -   [listTransactions][14]
        -   [Parameters][15]
    -   [postTransaction][16]
        -   [Parameters][17]
    -   [postTransactionSync][18]
        -   [Parameters][19]
    -   [postTransactionAsync][20]
        -   [Parameters][21]
    -   [postTransactionCommit][22]
        -   [Parameters][23]
    -   [searchAssets][24]
        -   [Parameters][25]
    -   [searchMetadata][26]
        -   [Parameters][27]
-   [Transaction][28]
    -   [serializeTransactionIntoCanonicalString][29]
        -   [Parameters][30]
    -   [makeCreateTransaction][31]
        -   [Parameters][32]
    -   [makeEd25519Condition][33]
        -   [Parameters][34]
    -   [makeOutput][35]
        -   [Parameters][36]
    -   [makeSha256Condition][37]
        -   [Parameters][38]
    -   [makeThresholdCondition][39]
        -   [Parameters][40]
    -   [makeTransferTransaction][41]
        -   [Parameters][42]
    -   [signTransaction][43]
        -   [Parameters][44]
-   [ccJsonLoad][45]
    -   [Parameters][46]
-   [ccJsonify][47]
    -   [Parameters][48]

## Ed25519Keypair

[src/Ed25519Keypair.js:16-21][49]

Type: [Object][50]

### Parameters

-   `seed` **[Buffer][51]?** A seed that will be used as a key derivation function

### Properties

-   `publicKey` **[string][52]** 
-   `privateKey` **[string][52]** 

## Connection

[src/connection.js:21-201][53]

### Parameters

-   `nodes`  
-   `headers` **[Object][50]** Common headers for every request (optional, default `{}`)
-   `timeout` **float** Optional timeout in secs (optional, default `DEFAULT_TIMEOUT`)

### getBlock

[src/connection.js:79-85][54]

#### Parameters

-   `blockHeight`  

### getTransaction

[src/connection.js:90-96][55]

#### Parameters

-   `transactionId`  

### listBlocks

[src/connection.js:102-108][56]

#### Parameters

-   `transactionId`  
-   `status`  

### listOutputs

[src/connection.js:114-126][57]

#### Parameters

-   `publicKey`  
-   `spent`  

### listTransactions

[src/connection.js:132-139][58]

#### Parameters

-   `assetId`  
-   `operation`  

### postTransaction

[src/connection.js:144-146][59]

#### Parameters

-   `transaction`  

### postTransactionSync

[src/connection.js:151-156][60]

#### Parameters

-   `transaction`  

### postTransactionAsync

[src/connection.js:162-167][61]

#### Parameters

-   `transaction`  

### postTransactionCommit

[src/connection.js:173-178][62]

#### Parameters

-   `transaction`  

### searchAssets

[src/connection.js:183-189][63]

#### Parameters

-   `search`  

### searchMetadata

[src/connection.js:194-200][64]

#### Parameters

-   `search`  

## Transaction

[src/transaction.js:16-258][65]

Construct Transactions

### serializeTransactionIntoCanonicalString

[src/transaction.js:22-29][66]

Canonically serializes a transaction into a string by sorting the keys

#### Parameters

-   `transaction`  
-   `null` **[Object][50]** (transaction)

Returns **[string][52]** a canonically serialized Transaction

### makeCreateTransaction

[src/transaction.js:80-87][67]

Generate a `CREATE` transaction holding the `asset`, `metadata`, and `outputs`, to be signed by
the `issuers`.

#### Parameters

-   `asset` **[Object][50]** Created asset's data
-   `metadata` **[Object][50]** Metadata for the Transaction
-   `outputs` **[Array][68]&lt;[Object][50]>** Array of Output objects to add to the Transaction.
                              Think of these as the recipients of the asset after the transaction.
                              For `CREATE` Transactions, this should usually just be a list of
                              Outputs wrapping Ed25519 Conditions generated from the issuers' public
                              keys (so that the issuers are the recipients of the created asset).
-   `issuers` **...[Array][68]&lt;[string][52]>** Public key of one or more issuers to the asset being created by this
                                 Transaction.
                                 Note: Each of the private keys corresponding to the given public
                                 keys MUST be used later (and in the same order) when signing the
                                 Transaction (`signTransaction()`).

Returns **[Object][50]** Unsigned transaction -- make sure to call signTransaction() on it before
                  sending it off!

### makeEd25519Condition

[src/transaction.js:96-107][69]

Create an Ed25519 Cryptocondition from an Ed25519 public key
to put into an Output of a Transaction

#### Parameters

-   `publicKey` **[string][52]** base58 encoded Ed25519 public key for the recipient of the Transaction
-   `json` **[boolean][70]** If true returns a json object otherwise a crypto-condition type (optional, default `true`)

Returns **[Object][50]** Ed25519 Condition (that will need to wrapped in an Output)

### makeOutput

[src/transaction.js:117-137][71]

Create an Output from a Condition.
Note: Assumes the given Condition was generated from a
single public key (e.g. a Ed25519 Condition)

#### Parameters

-   `condition` **[Object][50]** Condition (e.g. a Ed25519 Condition from `makeEd25519Condition()`)
-   `amount` **[string][52]** Amount of the output (optional, default `'1'`)

Returns **[Object][50]** An Output usable in a Transaction

### makeSha256Condition

[src/transaction.js:145-153][72]

Create a Preimage-Sha256 Cryptocondition from a secret to put into an Output of a Transaction

#### Parameters

-   `preimage` **[string][52]** Preimage to be hashed and wrapped in a crypto-condition
-   `json` **[boolean][70]** If true returns a json object otherwise a crypto-condition type (optional, default `true`)

Returns **[Object][50]** Preimage-Sha256 Condition (that will need to wrapped in an Output)

### makeThresholdCondition

[src/transaction.js:162-176][73]

Create an Sha256 Threshold Cryptocondition from threshold to put into an Output of a Transaction

#### Parameters

-   `threshold` **[number][74]** 
-   `subconditions` **[Array][68]**  (optional, default `[]`)
-   `json` **[boolean][70]** If true returns a json object otherwise a crypto-condition type (optional, default `true`)

Returns **[Object][50]** Sha256 Threshold Condition (that will need to wrapped in an Output)

### makeTransferTransaction

[src/transaction.js:199-220][75]

Generate a `TRANSFER` transaction holding the `asset`, `metadata`, and `outputs`, that fulfills
the `fulfilledOutputs` of `unspentTransaction`.

#### Parameters

-   `unspentOutputs`  
-   `outputs` **[Array][68]&lt;[Object][50]>** Array of Output objects to add to the Transaction.
                              Think of these as the recipients of the asset after the transaction.
                              For `TRANSFER` Transactions, this should usually just be a list of
                              Outputs wrapping Ed25519 Conditions generated from the public keys of
                              the recipients.
-   `metadata` **[Object][50]** Metadata for the Transaction
-   `unspentTransaction` **[Object][50]** Previous Transaction you have control over (i.e. can fulfill
                                       its Output Condition)
-   `OutputIndices` **...[number][74]** Indices of the Outputs in `unspentTransaction` that this
                                        Transaction fulfills.
                                        Note that listed public keys listed must be used (and in
                                        the same order) to sign the Transaction
                                        (`signTransaction()`).

Returns **[Object][50]** Unsigned transaction -- make sure to call signTransaction() on it before
                  sending it off!

### signTransaction

[src/transaction.js:233-257][76]

Sign the given `transaction` with the given `privateKey`s, returning a new copy of `transaction`
that's been signed.
Note: Only generates Ed25519 Fulfillments. Thresholds and other types of Fulfillments are left as
an exercise for the user.

#### Parameters

-   `transaction` **[Object][50]** Transaction to sign. `transaction` is not modified.
-   `privateKeys` **...[string][52]** Private keys associated with the issuers of the `transaction`.
                                   Looped through to iteratively sign any Input Fulfillments found in
                                   the `transaction`.

Returns **[Object][50]** The signed version of `transaction`.

## ccJsonLoad

[src/utils/ccJsonLoad.js:14-44][77]

Loads a crypto-condition class (Fulfillment or Condition) from a BigchainDB JSON object

### Parameters

-   `conditionJson` **[Object][50]** 

Returns **cc.Condition** Ed25519 Condition (that will need to wrapped in an Output)

## ccJsonify

[src/utils/ccJsonify.js:12-65][78]

Serializes a crypto-condition class (Condition or Fulfillment) into a BigchainDB-compatible JSON

### Parameters

-   `fulfillment` **cc.Fulfillment** base58 encoded Ed25519 public key for recipient of the Transaction

Returns **[Object][50]** Ed25519 Condition (that will need to wrapped in an Output)

[1]: #ed25519keypair

[2]: #parameters

[3]: #properties

[4]: #connection

[5]: #parameters-1

[6]: #getblock

[7]: #parameters-2

[8]: #gettransaction

[9]: #parameters-3

[10]: #listblocks

[11]: #parameters-4

[12]: #listoutputs

[13]: #parameters-5

[14]: #listtransactions

[15]: #parameters-6

[16]: #posttransaction

[17]: #parameters-7

[18]: #posttransactionsync

[19]: #parameters-8

[20]: #posttransactionasync

[21]: #parameters-9

[22]: #posttransactioncommit

[23]: #parameters-10

[24]: #searchassets

[25]: #parameters-11

[26]: #searchmetadata

[27]: #parameters-12

[28]: #transaction

[29]: #serializetransactionintocanonicalstring

[30]: #parameters-13

[31]: #makecreatetransaction

[32]: #parameters-14

[33]: #makeed25519condition

[34]: #parameters-15

[35]: #makeoutput

[36]: #parameters-16

[37]: #makesha256condition

[38]: #parameters-17

[39]: #makethresholdcondition

[40]: #parameters-18

[41]: #maketransfertransaction

[42]: #parameters-19

[43]: #signtransaction

[44]: #parameters-20

[45]: #ccjsonload

[46]: #parameters-21

[47]: #ccjsonify

[48]: #parameters-22

[49]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/Ed25519Keypair.js#L16-L21 "Source code on GitHub"

[50]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[51]: https://nodejs.org/api/buffer.html

[52]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[53]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L21-L201 "Source code on GitHub"

[54]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L79-L85 "Source code on GitHub"

[55]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L90-L96 "Source code on GitHub"

[56]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L102-L108 "Source code on GitHub"

[57]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L114-L126 "Source code on GitHub"

[58]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L132-L139 "Source code on GitHub"

[59]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L144-L146 "Source code on GitHub"

[60]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L151-L156 "Source code on GitHub"

[61]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L162-L167 "Source code on GitHub"

[62]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L173-L178 "Source code on GitHub"

[63]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L183-L189 "Source code on GitHub"

[64]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/connection.js#L194-L200 "Source code on GitHub"

[65]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L16-L258 "Source code on GitHub"

[66]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L22-L29 "Source code on GitHub"

[67]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L80-L87 "Source code on GitHub"

[68]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array

[69]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L96-L107 "Source code on GitHub"

[70]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean

[71]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L117-L137 "Source code on GitHub"

[72]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L145-L153 "Source code on GitHub"

[73]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L162-L176 "Source code on GitHub"

[74]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[75]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L199-L220 "Source code on GitHub"

[76]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/transaction.js#L233-L257 "Source code on GitHub"

[77]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/utils/ccJsonLoad.js#L14-L44 "Source code on GitHub"

[78]: https://github.com/bigchaindb/js-bigchaindb-driver/blob/b37bdcded5d62c90b86ec4ee5dade386a7388a85/src/utils/ccJsonify.js#L12-L65 "Source code on GitHub"
