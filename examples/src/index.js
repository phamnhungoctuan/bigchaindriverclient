var express = require("express");
var request = require('request');
var bodyParser = require('body-parser');
var moment = require('moment');
var app = express();
app.listen(3000);
app.use(bodyParser.json()); // support json encoded bodies
// app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

const driver = require('bigchaindb-driver')

// BigchainDB server instance (e.g. https://example.com/api/v1/)
const API_PATH = 'http://localhost:9984/api/v1/'

// Create a new keypair.
const alice = new driver.Ed25519Keypair()

app.post('/create', function(req, res) {
    var file_id = req.body.file_id;
    var key = req.body.key;
    var created_at = req.body.created_at;
    var config = req.body.config ? req.body.config : null;

    // Construct a transaction payload
    const tx = driver.Transaction.makeCreateTransaction(
        // Define the asset to store, in this example it is the current temperature
        // (in Celsius) for the city of Berlin.
        { file_id: file_id, key : key, created_at: created_at},

        // Metadata contains information about the transaction itself
        // (can be `null` if not needed)
        { what: config },

        // A transaction needs an output
        [ driver.Transaction.makeOutput(
                driver.Transaction.makeEd25519Condition(alice.publicKey))
        ],
        alice.publicKey
    )

    // Sign the transaction with private keys
    const txSigned = driver.Transaction.signTransaction(tx, alice.privateKey)

    // Send the transaction off to BigchainDB
    const conn = new driver.Connection(API_PATH)

    conn.postTransactionCommit(txSigned)
        .then(retrievedTx => {
            res.send({success: true, txtId: txSigned.id});
            console.log('Transaction ' + retrievedTx.id + ' successfully posted.');
        }) 
});

app.post('/search', function(req, res) {
    const conn = new driver.Connection(API_PATH)
    if(req.body.file_id){
        conn.searchAssets(req.body.file_id)
            .then(assets => {
                res.send(assets);
                //console.log('Found assets with serial number Bicycle Inc.:', assets))
            }); 
    }
});




